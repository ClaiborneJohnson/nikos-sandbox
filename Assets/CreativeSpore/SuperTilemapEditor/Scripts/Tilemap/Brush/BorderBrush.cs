﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System.Collections.Generic;

namespace CreativeSpore.SuperTilemapEditor
{
/**
 * BorderBrush is essentially an inverse to the CarpetBrush.  Instead of tiling inward to form a carpet, 
 * we want to tile outward to form a border.  Any tile in the tilemap the brush touches that isn't null and 
 * isn't part of the autotiling is treated as a tile to border around.
 * 
 * This is useful for e.g. boxing a room with directional walls by drawing the brush around the edges.
 * 
 * Since BorderBrush paints tiles to enclose exterior tiles rather than interior ones, we can keep most of the
 * logic from CarpetBrush and just invert the neighbor index whenever it is accessed.  The only other changes
 * required are to modify the autotiling_dir booleans to look outward and invert the s_showDiagonal booleans to 
 * match the exterior behavior.
 * 
 * Author: Claiborne Johnson, modified from CreativeSpore's CarpetBrush.cs
 * 
 * CreativeSpore is authorized to incorporate this code and any sensible modifications into SuperTilemapEditor
 * if desired, provided that my name is credited.
 * */
	public class BorderBrush : RoadBrush {
		public uint[] InteriorCornerTileIds = Enumerable.Repeat(Tileset.k_TileData_Empty, 4).ToArray();

		#region IBrush

		public override uint PreviewTileData()
		{
			return TileIds[6];
			//return TileIds[15] != Tileset.k_TileId_Empty ? TileIds[15] : TileIds[6]; //15 center brush (╬) ; 6 top left brush (╔)
		}

		static int s_brushId;
		static int s_neighIdx;
		static uint s_tileData;
		static bool[] s_showDiagonal = new bool[4];
		static bool s_needsSubTiles;

		private void CalculateNeighbourData(STETilemap tilemap, int gridX, int gridY, uint tileData)
		{
			s_needsSubTiles = false;
			s_brushId = (int)((tileData & Tileset.k_TileDataMask_BrushId) >> 16);

			// BorderBrush's use case often involves bordering tiles in other maps, so grab them all from the TileMapGroup if one exists
			var tilemaps = tilemap.ParentTilemapGroup?.Tilemaps ?? new List<STETilemap> { tilemap };

			// BorderBrush borders a tile position if it's touching a valid tile there and not autotiling with it for any map in the group
			bool bordering_N = tilemaps.Any(map => (map.GetTile(gridX, gridY + 1) != null) && !AutotileWith(map, s_brushId, gridX, gridY + 1));
			bool bordering_E = tilemaps.Any(map => (map.GetTile(gridX + 1, gridY) != null) && !AutotileWith(map, s_brushId, gridX + 1, gridY));
			bool bordering_S = tilemaps.Any(map => (map.GetTile(gridX, gridY - 1) != null) && !AutotileWith(map, s_brushId, gridX, gridY - 1));
			bool bordering_W = tilemaps.Any(map => (map.GetTile(gridX - 1, gridY) != null) && !AutotileWith(map, s_brushId, gridX - 1, gridY));	

			s_neighIdx = 0;
			if (bordering_N) s_neighIdx |= 1;
			if (bordering_E) s_neighIdx |= 2;
			if (bordering_S) s_neighIdx |= 4;
			if (bordering_W) s_neighIdx |= 8;

			// BorderBrush tiles are placed inversely to neighbors compared to CarpetBrush
			s_needsSubTiles = (s_neighIdx == (15-0) || s_neighIdx == (15-1) || s_neighIdx == (15-2) || s_neighIdx == (15-4)
				|| s_neighIdx == (15-5) || s_neighIdx == (15-8) || s_neighIdx == (15-10)) ;

			// diagonals
			{
				// old CarpetBrush logic: 
				// if we haven't found multiple diagonals and the tile is surrounded on all adjacents, we can pick an inner corner
				// otherwise if we are cornering multiple tiles or need to tile with a corner and adjacent sides, we need subtiles

				// new BorderBrush logic: 
				// if we haven't found multiple diagonals and the tile has no adjacents, we can pick an inner corner
				// otherwise if we are cornering multiple tiles or need to border a corner and non-adjacent sides, we need subtiles

				bool bordering_NE = tilemaps.Any(map => (map.GetTile(gridX + 1, gridY + 1) != null) && !AutotileWith(map, s_brushId, gridX + 1, gridY + 1));
				bool bordering_SE = tilemaps.Any(map => (map.GetTile(gridX + 1, gridY - 1) != null) && !AutotileWith(map, s_brushId, gridX + 1, gridY - 1));
				bool bordering_SW = tilemaps.Any(map => (map.GetTile(gridX - 1, gridY - 1) != null) && !AutotileWith(map, s_brushId, gridX - 1, gridY - 1));
				bool bordering_NW = tilemaps.Any(map => (map.GetTile(gridX - 1, gridY + 1) != null) && !AutotileWith(map, s_brushId, gridX - 1, gridY + 1));

				// if we're bordering one of the corner's sides then the corner is already drawn
				s_showDiagonal[0] = bordering_SW && !bordering_S && !bordering_W;
				s_showDiagonal[1] = bordering_SE && !bordering_S && !bordering_E;
				s_showDiagonal[2] = bordering_NW && !bordering_N && !bordering_W;
				s_showDiagonal[3] = bordering_NE && !bordering_N && !bordering_E;

				// using inverse index for tileIds lets us keep the same tile arrangement
				s_tileData = TileIds[15-s_neighIdx];
				bool foundTrueDiagonal = false;
				for (int i = 0; !s_needsSubTiles && i < s_showDiagonal.Length; ++i)
				{
					if (s_showDiagonal[i])
					{
						// if only a diagonal is true and it has no adjacent tiles, we don't need subtiles, instead the right corner tile will be taken
						s_needsSubTiles = foundTrueDiagonal || s_neighIdx != (15-15);
						foundTrueDiagonal = true;
						if (!s_needsSubTiles)
						{
							s_tileData = InteriorCornerTileIds[InteriorCornerTileIds.Length - i - 1];
						}
					}
				}                
			}
		}

		public override uint Refresh(STETilemap tilemap, int gridX, int gridY, uint tileData)
		{
			CalculateNeighbourData(tilemap, gridX, gridY, tileData);


			uint brushTileData = RefreshLinkedBrush(tilemap, gridX, gridY, s_tileData);
			// overwrite brush id
			brushTileData &= ~Tileset.k_TileDataMask_BrushId;
			brushTileData |= tileData & Tileset.k_TileDataMask_BrushId;   
			return brushTileData;
		}


		// '°', '├', '═', '┤', | 0, 2, 10, 8,
		// '┬', '╔', '╦', '╗', | 4, 6, 14, 12,
		// '║', '╠', '╬', '╣', | 5, 7, 15, 13,
		// '┴', '╚', '╩', '╝', | 1, 3, 11, 9,
		public override uint[] GetSubtiles(STETilemap tilemap, int gridX, int gridY, uint tileData)
		{
			CalculateNeighbourData(tilemap, gridX, gridY, tileData);
			// tiles that need subtile division
			if (s_needsSubTiles)
			{
				uint[] aSubTileData = null;

				// BorderBrush tiles are placed inversely to neighbors compared to CarpetBrush
				if (s_neighIdx == 15-0) //°
				{
					aSubTileData = new uint[] { TileIds[3], TileIds[9], TileIds[6], TileIds[12] };
				}
				else if (s_neighIdx == 15-4)//┬
				{
					aSubTileData = new uint[] { TileIds[6], TileIds[12], TileIds[6], TileIds[12] };
				}
				else if (s_neighIdx == 15-5)//║
				{
					aSubTileData = new uint[] { TileIds[7], TileIds[13], TileIds[7], TileIds[13] };
				}
				else if (s_neighIdx == 15-1)//┴
				{
					aSubTileData = new uint[] { TileIds[3], TileIds[9], TileIds[3], TileIds[9] };
				}
				else if (s_neighIdx == 15-2)//├
				{
					aSubTileData = new uint[] { TileIds[3], TileIds[3], TileIds[6], TileIds[6] };
				}
				else if (s_neighIdx == 15-10)//═
				{
					aSubTileData = new uint[] { TileIds[11], TileIds[11], TileIds[14], TileIds[14] };
				}
				else if (s_neighIdx == 15-8)//┤
				{
					aSubTileData = new uint[] { TileIds[9], TileIds[9], TileIds[12], TileIds[12] };
				}
				// NOTE: this case '╬' cut the tiles different (using corner tiles). 
				// If it is commented, and default case is used, instead or corner tiles, it will use the center tile '╬'
				// Depending on the graphics it could be interesting add a check box to choose between using this or not.
//				else if (s_neighIdx == 15-15)// ╬
//				{
//					aSubTileData = new uint[] { InteriorCornerTileIds[0], InteriorCornerTileIds[1], InteriorCornerTileIds[2], InteriorCornerTileIds[3] };
//				}
				else
				{
					aSubTileData = new uint[] { TileIds[15-s_neighIdx], TileIds[15-s_neighIdx], TileIds[15-s_neighIdx], TileIds[15-s_neighIdx] };
				}

				for (int i = 0; i < s_showDiagonal.Length; ++i)
				{
					aSubTileData[i] = RefreshLinkedBrush(tilemap, gridX, gridY, aSubTileData[i]);
					if (s_showDiagonal[i])
					{
						aSubTileData[i] = InteriorCornerTileIds[3 - i];
					}
					// Add animated tiles
					{
						TilesetBrush brush = Tileset.FindBrush(Tileset.GetBrushIdFromTileData(aSubTileData[i]));
						if (brush && brush.IsAnimated())
						{
							TilemapChunk.RegisterAnimatedBrush(brush, i);
						}
					}
				}

				return aSubTileData;
			}

			// Add animated tiles
			{
				TilesetBrush brush = Tileset.FindBrush(Tileset.GetBrushIdFromTileData(s_tileData));
				if (brush && brush.IsAnimated())
				{
					TilemapChunk.RegisterAnimatedBrush(brush);
				}
			}
			return null;
		}



		//TODO: add cache for each subTile combination?
		static List<Vector2> s_mergedColliderVertexList = new List<Vector2>();
		public override Vector2[] GetMergedSubtileColliderVertices(STETilemap tilemap, int gridX, int gridY, uint tileData)
		{
			uint[] subTiles = GetSubtiles(tilemap, gridX, gridY, tileData);
			if (subTiles != null)
			{
				s_mergedColliderVertexList.Clear();
				for(int i = 0; i < subTiles.Length; ++i)
				{
					uint subTileData = subTiles[i];
					Tile tile = tilemap.Tileset.GetTile(Tileset.GetTileIdFromTileData(subTiles[i]));
					if(tile != null && tile.collData.type != eTileCollider.None)
					{
						TileColliderData tileCollData = tile.collData;
						if ((subTileData & (Tileset.k_TileFlag_FlipH | Tileset.k_TileFlag_FlipV | Tileset.k_TileFlag_Rot90)) != 0)
						{
							tileCollData = tileCollData.Clone();
							tileCollData.ApplyFlippingFlags(subTileData);
						}
						Vector2[] vertices = tile.collData.GetVertices();
						if (vertices != null)
						{
							for (int v = 0; v < vertices.Length; ++v)
							{
								Vector2 v0, v1;
								if (v < tile.collData.vertices.Length - 1)
								{
									v0 = vertices[v];
									v1 = vertices[v + 1];
								}
								else
								{
									v0 = vertices[v];
									v1 = vertices[0];
								}

								if(i == 0 || i == 2) //left side
								{
									if (v0.x >= .5f && v1.x >= .5f) continue;
									float newY = v0.y + (.5f - v0.x) * (v1.y - v0.y) / (v1.x - v0.x);
									if (v0.x > .5f)
									{
										v0.y = newY;
										v0.x = .5f;
									}
									else if(v1.x > .5f)
									{
										v1.y = newY;
										v1.x = .5f;
									}
								}
								else // right side
								{
									if (v0.x <= .5f && v1.x <= .5f) continue;
									float newY = v0.y + (.5f - v0.x) * (v1.y - v0.y) / (v1.x - v0.x);
									if (v0.x < .5f)
									{
										v0.y = newY;
										v0.x = .5f;
									}
									else if (v1.x < .5f)
									{
										v1.y = newY;
										v1.x = .5f;
									}
								}

								if (i == 0 || i == 1) //bottom side
								{
									if (v0.y >= .5f && v1.y >= .5f) continue;
									float newX = v0.x + (.5f - v0.y) * (v1.x - v0.x) / (v1.y - v0.y);
									if (v0.y > .5f)
									{
										v0.x = newX;
										v0.y = .5f;
									}
									else if (v1.y > .5f)
									{
										v1.x = newX;
										v1.y = .5f;
									}
								}
								else // top side
								{
									if (v0.y <= .5f && v1.y <= .5f) continue;
									float newX = v0.x + (.5f - v0.y) * (v1.x - v0.x) / (v1.y - v0.y);
									if (v0.y < .5f)
									{
										v0.x = newX;
										v0.y = .5f;
									}
									else if (v1.y < .5f)
									{
										v1.x = newX;
										v1.y = .5f;
									}
								}

								s_mergedColliderVertexList.Add(v0);
								s_mergedColliderVertexList.Add(v1);
							}
						}
					}
				}
				return s_mergedColliderVertexList.ToArray();
			}
			return null;
		}

		#endregion

	}
}