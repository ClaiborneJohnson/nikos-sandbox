﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

/*
 * Trigger that fires if the player attempts to move into it (even if blocked).
 * 
 * Note that the object's transform position needs to match the player's transform position exactly.
 * 
 * A coroutine is run to wait for player input once the player is found, because otherwise the player may
 * have already moved partially off the transform once the script catches the axis input.
 * 
*/
public class PlayerPushTrigger : MonoBehaviour
{

	public bool pushLeft = true;
	public bool pushRight = true;
	public bool pushUp = true;
	public bool pushDown = true;
	public UnityEvent eventToInvoke;

	private PlayerController playerCtrl;

	void Start ()
	{
		playerCtrl = GameObject.FindObjectOfType<PlayerController> ();
	}

	void Update ()
	{
		// nothing to check for if no player
		if (playerCtrl == null)
			return;

		// if player is in position for the push we want, wait for them to give input
		if (pushLeft && playerCtrl.transform.position == transform.position + Vector3.right ||
		    pushRight && playerCtrl.transform.position == transform.position + Vector3.left ||
		    pushUp && playerCtrl.transform.position == transform.position + Vector3.down ||
		    pushDown && playerCtrl.transform.position == transform.position + Vector3.up) {
			StartCoroutine (WaitForInput ());
		}

	}

	private IEnumerator WaitForInput ()
	{
		// spin every frame until the player moves
		while (Input.GetAxisRaw ("Horizontal") == 0 && Input.GetAxisRaw ("Vertical") == 0) {
			yield return null;
		}

		// if the move matches a push we want, invoke the event
		if (pushLeft && Input.GetAxisRaw ("Horizontal") < 0 ||
		    pushRight && Input.GetAxisRaw ("Horizontal") > 0 ||
		    pushUp && Input.GetAxisRaw ("Vertical") > 0 ||
		    pushDown && Input.GetAxisRaw ("Vertical") < 0) {
			eventToInvoke.Invoke ();
		}
			
		// otherwise back to update function next frame
		yield return 0;
	}

}
