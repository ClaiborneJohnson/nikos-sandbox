﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class P_Trigger : MonoBehaviour {

	public UnityEvent pEvent;

	// Update is called once per frame
	void Update () {

		if (Input.GetKeyDown("p"))
			pEvent.Invoke();
	}
}
