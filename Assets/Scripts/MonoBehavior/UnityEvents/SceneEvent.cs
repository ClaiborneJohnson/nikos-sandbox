﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneEvent : MonoBehaviour {

	public string sceneName;                    // The name of the scene to be loaded.
	public string startingPointInLoadedScene;   // The name of the StartingPosition in the newly loaded scene.
	public SaveData playerSaveData;             // Reference to the save data asset that will store the StartingPosition.

	private SceneController sceneController;    // Reference to the SceneController to actually do the loading and unloading of scenes.

	// Use this for initialization
	void Awake () {
		sceneController = FindObjectOfType<SceneController> ();
	}

	public void SaveAndChangeScenes () {
		// Save the StartingPosition's name to the data asset.
		playerSaveData.Save (PlayerController.startingPositionKey, startingPointInLoadedScene);

		// Start the scene loading process.
		sceneController.FadeAndLoadScene (this);
	}
}
