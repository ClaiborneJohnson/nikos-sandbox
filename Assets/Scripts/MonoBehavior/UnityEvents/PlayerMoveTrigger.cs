﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

/*
 * Trigger that fires if the player attempts to move a certain direction while standing on it.
 * 
 * Note that the object's transform position needs to match the player's transform position exactly.
 * 
 * A coroutine is run to wait for player input once the player is found, because otherwise the player may
 * have already moved partially off the transform once the script catches the axis input.
 * 
*/
public class PlayerMoveTrigger : MonoBehaviour {

	public bool onLeft;
	public bool onRight;
	public bool onUp;
	public bool onDown;
	public UnityEvent eventToInvoke;

	private PlayerController playerCtrl;

	void Start () {
		playerCtrl = GameObject.FindObjectOfType<PlayerController>();
	}

	void Update () {
		// nothing to check for if no player
		if (playerCtrl == null)
			return;

		// if player is close enough, wait for them to give input
		if (transform.position == playerCtrl.transform.position) {
			StartCoroutine (WaitForInput());
		}
			
	}
		
	private IEnumerator WaitForInput() {
		// spin every frame until the player moves
		while (Input.GetAxisRaw("Horizontal") == 0 && Input.GetAxisRaw("Vertical") == 0) {
			yield return null;
		}
			
		// if the move matches one we want, invoke the event
		if (onLeft && Input.GetAxisRaw ("Horizontal") < 0 ||
			onRight && Input.GetAxisRaw ("Horizontal") > 0 ||
			onUp && Input.GetAxisRaw ("Vertical") > 0 ||
			onDown && Input.GetAxisRaw ("Vertical") < 0) {
			eventToInvoke.Invoke ();
		}

		// otherwise back to update function next frame
		yield return 0;
	}
		
}
