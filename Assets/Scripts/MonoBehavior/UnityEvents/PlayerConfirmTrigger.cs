﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

/*
 * Trigger that fires if the player presses confirm while facing it on an adjacent tile.
 * 
 * Note that the transform positions needs to be precisely adjacent and the animator needs float parameters
 * for the direction the player is facing (e.g. blendX = 0.0f and blendY = 1.0f means they're facing up).
 * 
 * Currently can fire while moving through an object and pressing confirm.  
 * This could be at least partially fixed by checking an isMoving bool in the animator but this seems minor.
 * 
 * No coroutine is needed to handle the player moving away because the trigger is a button and not movement.
 * 
*/
public class PlayerConfirmTrigger : MonoBehaviour {

	public UnityEvent eventToInvoke;

	public const string confirmButton = "Submit";
	public const string blendX = "inputX";
	public const string blendY = "inputY";

	private PlayerController playerCtrl;
	private Animator playerAnim;

	// Use this for initialization
	void Start () {
		playerCtrl = GameObject.FindObjectOfType<PlayerController>();
		playerAnim = playerCtrl.GetComponentInParent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
		// nothing to check for if no player
		if (playerCtrl == null || playerAnim == null) {
			return;
		}

		// if player is adjacent and facing this and hits confirm, invoke the method
		if (transform.position + Vector3.up == playerCtrl.transform.position && 
			playerAnim.GetFloat(blendX) == 0.0f && playerAnim.GetFloat(blendY) == -1.0f && Input.GetButtonDown(confirmButton)) {
			eventToInvoke.Invoke ();
		} else if (transform.position + Vector3.down == playerCtrl.transform.position && 
			playerAnim.GetFloat(blendX) == 0.0f && playerAnim.GetFloat(blendY) == 1.0f && Input.GetButtonDown(confirmButton)) {
			eventToInvoke.Invoke ();
		}
		else if (transform.position + Vector3.left == playerCtrl.transform.position && 
			playerAnim.GetFloat(blendX) == 1.0f && playerAnim.GetFloat(blendY) == 0.0f && Input.GetButtonDown(confirmButton)) {
			eventToInvoke.Invoke ();
		}
		else if (transform.position + Vector3.right == playerCtrl.transform.position && 
			playerAnim.GetFloat(blendX) == -1.0f && playerAnim.GetFloat(blendY) == 0.0f && Input.GetButtonDown(confirmButton)) {
			eventToInvoke.Invoke ();
		}

	}

}
