﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Logger : MonoBehaviour {
	public void Log(string s) {
		Debug.Log (s);
	}

	public void LogWarning(string s){
		Debug.LogWarning (s);
	}

	public void LogError(string s) {
		Debug.LogError (s);
	}
}
