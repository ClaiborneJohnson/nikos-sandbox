﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.Events;

// This script exists in the Persistent scene and manages the 
// dialogue box in the UI layer.  It will open the dialogue 
// box with the requested content when necessary, progress
// through the required lines, and close it.  Use serialization here.
public class DialogueController : MonoBehaviour {

	public event Action BeforeDialogueOpen;          // Event delegate that is called just before a dialogue opens.
	public event Action AfterDialogueClose;          // Event delegate that is called just after a dialogue closes.

	public CanvasGroup dialogueCanvasGroup;			 // The CanvasGroup for the dialogue box.

	public Dialogue dialogueAsset;

	// Use this for initialization
	void Start () {
		// dialogue box should start off invisible
		dialogueCanvasGroup.alpha = 0f;
	}
	
	// Probably use this to update box when open, may want a separate display class
//	void Update () {
//		
//	}

	// Called to open the dialogue box and display the given text
	// TODO: should have a function take serialized input, perhaps from another controller
	public void OpenTestDialogue(string displayText) {
		// input should provide speaker, text to set
		// perhaps serialization includes speaker info.. parsing logic could handle

		// If this event has any subscribers, call it.
		if (BeforeDialogueOpen != null)
			BeforeDialogueOpen ();

		// set text here, probably begin a coroutine to begin displaying the text?  maybe after visible..
		// need the speaker set before making box visible, text box set to empty

		// make dialogue box visible
		dialogueCanvasGroup.alpha = 1f;
	}

	// public void OpenDialogue(??? dialogueIndex) {
	   // load dialogueElement from dialogueAsset, this will be called with the appropriate index by a trigger
	   // set panel speaker to match dialogueElement's speaker
	   // start blip routine to parse and display text
	// }

//	private void blip(??? dialogueText) {
//		// parse dialogueText and display character by character
//
//		// probably move this to another file?
//	}

	public void CloseDialogue() {
		// make dialogue box invisible
		dialogueCanvasGroup.alpha = 0f;

		// If this event has any subscribers, call it.
		if (AfterDialogueClose != null)
			AfterDialogueClose ();
	}
}
