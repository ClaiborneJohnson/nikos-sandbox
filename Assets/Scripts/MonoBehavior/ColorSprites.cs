﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorSprites : MonoBehaviour {


	public Color house_color = new Color (240f, 240f, 240f, 1f);
	public Color green_color = new Color (225f, 235f, 225f, 1f);
	public Color blue_color = new Color (215f, 215f, 215f, 1f);

	// Use this for initialization
	void Start () {
		colorChildren (house_color);
	}
	
//	// Update is called once per frame
//	void Update () {
//		
//	}

	// Called to color all sprites for this object and its children
	// used to darken sprites
	void colorChildren(Color c) {
		// Find all of the sprite renderers on this object and its children
		SpriteRenderer[] otherRenderers = GetComponentsInChildren<SpriteRenderer> ();

		foreach (SpriteRenderer s in otherRenderers) {
			s.color = c;
		}
	}

	// Color a given game object's sprite
	void colorSprite(GameObject ob, Color c) {
		SpriteRenderer render = ob.GetComponent<SpriteRenderer> ();

		render.color = c;
	}

}
