﻿using System.Collections;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.EventSystems;

/*
 * Movement logic adapted from GridMove.
 * 
 * 
 * 
*/

[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(BoxCollider2D))]
[RequireComponent(typeof(SpriteRenderer))]
public class PlayerController : MonoBehaviour
{
	public Animator animator;			// Reference to the animator component.
	public BoxCollider2D boxCollider2D; // Reference to the boxcollider2d component.
	public LayerMask blockingLayer;		// layer on which we check collision
	public SaveData playerSaveData;     // Reference to the save data asset containing the player's starting position.
	public float moveSpeed = 6f;		// Speed at which the player moves
	public AudioClip moveSoundWood;		// footstep audio on wood surfaces TODO: move elsewhere

	private float gridSize = 1f;
	private Vector2 input;
	private bool isMoving = false;
//	private bool handleInput = true;    // Whether input is currently being handled.  (may not need this)
	private Vector3 startPosition;
	private Vector3 endPosition;	              

//    private WaitForSeconds inputHoldWait;       // The WaitForSeconds used to make the user wait before input is handled again.

    public const string startingPositionKey = "starting position";
                                                // The key used to retrieve the starting position from the playerSaveData.



    private void Start()
    {
		animator = GetComponent<Animator>();
		boxCollider2D = GetComponent<BoxCollider2D> ();

        // Load the starting position from the save data and find the transform from the starting position's name.
        string startingPositionName = "";
		string startingDirection = "";
        playerSaveData.Load(startingPositionKey, ref startingPositionName);
		Transform startingPosition = StartingPosition.FindStartingPositionAndDirection(startingPositionName, out startingDirection);

        // Set the player's position based on the starting position.
        transform.position = startingPosition.position;

		// Set the player's direction based on the starting direction.
		switch (startingDirection.ToLower()) {
		case "up":
			updateAnimator (0.0f, 1.0f); 
			break;
		case "down": 
			updateAnimator (0.0f, -1.0f);
			break;
		case "left": 
			updateAnimator (-1.0f, 0.0f);
			break;
		case "right": 
			updateAnimator (1.0f, 0.0f);
			break;
		}

    }
		
    private void Update()
    {
		// add code to grab confirm, etc
		// button interaction should probably take priority over movement?
		// can you e.g. open a menu while moving?  check oneshot

		// get player input, raw will only return -1, 0, 1
		input = new Vector2 (Input.GetAxisRaw ("Horizontal"), Input.GetAxisRaw ("Vertical"));

		// y gets priority on diagonals, not how it works in OneShot
		// use prior lastmove value?  
		// issue?: what if it went from 0,0 to 1,1.  Then there was no newest?
		// check how OneShot handles this.
		if (input.x != 0 && input.y != 0) {
			input.x = 0;  
			// TODO: in OneShot, newest input gets priority
			//				float lastX = animator.GetFloat ("LastMoveX");
			//				float lastY = animator.GetFloat ("LastMoveY");
			//				if (lastX > lastY) {
			//					input.y = 0;
			//				} else {
			//					input.x = 0;
			//				}
		}
			

		// Attempt to move if we got input and aren't already moving
		if (!isMoving && (input.x != 0 || input.y != 0)) {
			StartCoroutine (Move (transform));
		// Unset movement animation if we stop trying to move
		} else if (!isMoving) {
			animator.SetBool ("isMoving", false);  
		}
    }

	// tell animator direction Niko is trying to move
	void updateAnimator(float horizInput, float vertInput) {		
		animator.SetFloat ("inputX", horizInput);
		animator.SetFloat ("inputY", vertInput);
		animator.SetFloat ("lastMoveX", horizInput);
		animator.SetFloat ("lastMoveY", vertInput);
	}

	private IEnumerator Move(Transform transform) {
		isMoving = true;

		// tell animator direction Niko is trying to move
		updateAnimator (input.x, input.y);

		// calculate start and end positions
		startPosition = transform.position;
		// endPosition = (start +- 1, y +- 1, 0)
		endPosition = new Vector3(startPosition.x + System.Math.Sign(input.x) * gridSize,
			startPosition.y + System.Math.Sign(input.y) * gridSize, startPosition.z);

		// cast a ray to see if player would collide with something
		boxCollider2D.enabled = false;
		RaycastHit2D hit = Physics2D.Linecast (startPosition, endPosition, blockingLayer);
		boxCollider2D.enabled = true;

		// only move if player doesn't collide with something, note this doesn't work with trigger colliders
		// cause it will hit the trigger's collider and not move.  If already inside it, it will hit it as well.
		// perhaps replacing the raycast with a boxcast would fix this issue.
		// checking the tiles directly may be smarter but misbehaves on prefabs, which we sometimes need (e.g. pc).
		if (hit.transform == null) {
			// Set movement animation once we start moving
			animator.SetBool ("isMoving", true);

			float factor;
			// Run is inverse by default, maybe add option for this later
			if (Input.GetButton ("Run")) {
				factor = .5f;
			} else {
				factor = 1f;
			}

			// take a step by linearly interpolating between start and end positions
			float t = 0;
			while (t < 1f) {
				t += Time.deltaTime * (moveSpeed / gridSize) * factor;
				transform.position = Vector3.Lerp (startPosition, endPosition, t);
				yield return null; // runs this loop again next frame
			}

			// play move sound after taking a step
			// TODO: footsteps are different on different backgrounds, check tags on the tiles we move to
			// TODO: plays too often, OneShot behavior skips steps sometimes?
			// probably needs a timer so that the sound doesn't play more than once a second or something
			// probably make this a trigger to send to another script, include the tile stepped on
			SoundManager.instance.PlaySingle (moveSoundWood);
		} else {
			// Unset movement animation trigger if we collide with something
			animator.SetBool ("isMoving", false);
		}
			
		isMoving = false;
		yield return 0;
	}
}
	

    // This function is called by the EventTrigger on the scene's ground when it is clicked on.
//    public void OnGroundClick(BaseEventData data)
//    {
//        // If the handle input flag is set to false then do nothing.
//        if(!handleInput)
//            return;
//        
//        // The player is no longer headed for an interactable so set it to null.
//        currentInteractable = null;
//
//        // This function needs information about a click so cast the BaseEventData to a PointerEventData.
//        PointerEventData pData = (PointerEventData)data;
//
//        // Try and find a point on the nav mesh nearest to the world position of the click and set the destination to that.
//        NavMeshHit hit;
//        if (NavMesh.SamplePosition (pData.pointerCurrentRaycast.worldPosition, out hit, navMeshSampleDistance, NavMesh.AllAreas))
//            destinationPosition = hit.position;
//        else
//            // In the event that the nearest position cannot be found, set the position as the world position of the click.
//            destinationPosition = pData.pointerCurrentRaycast.worldPosition;
//
//        // Set the destination of the nav mesh agent to the found destination position and start the nav mesh agent going.
//        agent.SetDestination(destinationPosition);
//        agent.isStopped = false;
//    }


    // This function is called by the EventTrigger on an Interactable, the Interactable component is passed into it.
//    public void OnInteractableClick(Interactable interactable)
//    {
//        // If the handle input flag is set to false then do nothing.
//        if(!handleInput)
//            return;
//
//        // Store the interactble that was clicked on.
//        currentInteractable = interactable;
//
//        // Set the destination to the interaction location of the interactable.
//        destinationPosition = currentInteractable.interactionLocation.position;
//
//        // Set the destination of the nav mesh agent to the found destination position and start the nav mesh agent going.
//        agent.SetDestination(destinationPosition);
//        agent.isStopped = false;
//    }


//    private IEnumerator WaitForInteraction ()
//    {
//        // As soon as the wait starts, input should no longer be accepted.
//        handleInput = false;
//
//        // Wait for the normal pause on interaction.
//        yield return inputHoldWait;
//
//        // Until the animator is in a state with the Locomotion tag, wait.
//        while (animator.GetCurrentAnimatorStateInfo (0).tagHash != hashLocomotionTag)
//        {
//            yield return null;
//        }
//
//        // Now input can be accepted again.
//        handleInput = true;
//    }