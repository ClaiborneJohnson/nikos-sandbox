﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Smoothly changes a transform to follow a target over time
public class SmoothFollow : MonoBehaviour {
	public Transform target;								// the target transform to follow
	public float smoothTime = 0.3F;							// approximately how long it will take to reach the target
	public Vector3 relativeToTarget = new Vector3(0,0,1);	// where we want to end up relative to the target

	private Vector3 velocity = Vector3.zero;	// initial velocity (changed by update)

	void Update() {
		// Define a targetPosition relative to the target
		Vector3 targetPosition = target.TransformPoint(relativeToTarget);

		// Smoothly move towards that target position
		transform.position = Vector3.SmoothDamp(transform.position, targetPosition, ref velocity, smoothTime);
	}
}
