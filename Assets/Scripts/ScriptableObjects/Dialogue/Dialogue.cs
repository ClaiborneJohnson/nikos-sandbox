﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using UnityEngine;

[Serializable]
public class Dialogue : ScriptableObject
{
	public List<DialogueEntry> Entries = new List<DialogueEntry>();
}
