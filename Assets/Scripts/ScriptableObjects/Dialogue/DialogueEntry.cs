﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class DialogueEntry : ScriptableObject {
	// The name of the speaker of the dialogue, used to format display or load the appropriate face sprite
	public string SpeakerName;
	// The text to display when the dialogue entry is opened (including formatting)
	public string EntryText;
}
