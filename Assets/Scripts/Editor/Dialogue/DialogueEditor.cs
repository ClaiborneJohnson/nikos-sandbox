﻿using System;
using UnityEngine;
using UnityEditor;

[CanEditMultipleObjects]
[CustomEditor(typeof(Dialogue))]
public class DialogueEditor : Editor {
	private Dialogue _dialogue; // Reference to the target

	// better than an enum would be to select the sprite, then fill in the code
	public enum FACE_OPTIONS
	{
		ed = 0,
		unknown = 1,
		niko,
		niko2,
		niko3,
		niko4,
		niko5,
		niko6,
		niko_83c,
		niko_cry,
		niko_distressed,
		niko_distressed2,
		niko_distressed_talk,
		niko_eyeclosed,
		niko_eyeclosed2,
		niko_gasmask,
		niko_huh,
		niko_less_sad,
		niko_pancakes,
		niko_sad,
		niko_shock,
		niko_smile,
		niko_speak,
		niko_surprised,
		niko_upset,
		niko_upset2,
		niko_upset_meow,
		niko_what,
		niko_what2,
		niko_wtf,
		niko_wtf2,
		niko_yawn
	}
	public FACE_OPTIONS face_options;
	// display an enum popup, perhaps make tiny next to field as usability improvement
	//	face_options = (FACE_OPTIONS)EditorGUILayout.EnumPopup ("Speaker:", face_options);

	private void OnEnable() {
		// cache the reference to the target
		_dialogue = (Dialogue)target;
	}

	public override void OnInspectorGUI() {
		serializedObject.Update();

		// Display a label for this data type
		EditorGUILayout.LabelField("Dialogue Entries");

		// Surround the data type in a box
		EditorGUILayout.BeginVertical(GUI.skin.box);
		//	EditorGUI.indentLevel++;

		// Iterate over dialogue entries
		for (int i = 0; i < _dialogue.Entries.Count; i++)
		{
			// display a label for each entry (currently just their number, TODO: make this collapsable)
			EditorGUILayout.LabelField("Entry " + i);

			// begin speaker sprite + name row
			EditorGUILayout.BeginHorizontal();

			// display the sprite for the current speaker name		
			EditorGUI.BeginChangeCheck();
			var spriteObject = EditorGUILayout.ObjectField(GetSpriteFromSpeakerName(_dialogue.Entries[i].SpeakerName), typeof(Sprite), false, GUILayout.Height(96), GUILayout.Width(96));
			// if a new sprite is selected, update the speakerName to match
			if (EditorGUI.EndChangeCheck() && spriteObject != null)
			{
				_dialogue.Entries[i].SpeakerName = "@" + spriteObject.name;
			}

			// begin speaker name column
			// note: sprite will update to match since it is determined from this
			EditorGUILayout.BeginVertical();
			EditorGUILayout.LabelField("Speaker Name:");
			_dialogue.Entries[i].SpeakerName = EditorGUILayout.TextField(_dialogue.Entries[i].SpeakerName);
			EditorGUILayout.LabelField("Speaker Name Presets:");
			EditorGUILayout.BeginHorizontal();
			if (GUILayout.Button("faceless", GUILayout.MaxWidth(75)))
			{
				_dialogue.Entries[i].SpeakerName = "@unknown";
			};
			if (GUILayout.Button("narration", GUILayout.MaxWidth(75)))
			{
				_dialogue.Entries[i].SpeakerName = "@ed";
			};
			EditorGUILayout.EndHorizontal();
			EditorGUILayout.BeginHorizontal();
			if (GUILayout.Button("previous", GUILayout.MaxWidth(75)) && i > 0)
			{  // TODO: else, fadeout this button
				_dialogue.Entries[i].SpeakerName = _dialogue.Entries[i - 1].SpeakerName;
			};
			if (GUILayout.Button("next", GUILayout.MaxWidth(75)) && i < _dialogue.Entries.Count - 1)
			{  // TODO: else, fadeout this button
				_dialogue.Entries[i].SpeakerName = _dialogue.Entries[i + 1].SpeakerName;
			};
			EditorGUILayout.EndHorizontal();
			EditorGUILayout.EndVertical();

			EditorGUILayout.EndHorizontal();

			// display the entry text
			EditorGUILayout.LabelField("Entry Text:");
			_dialogue.Entries[i].EntryText = EditorGUILayout.TextArea(_dialogue.Entries[i].EntryText, GUILayout.MinHeight(30));
			EditorGUILayout.LabelField("Formatting Presets:");
			EditorGUILayout.BeginHorizontal();
			if (GUILayout.Button("Player", GUILayout.MaxWidth(75)))
			{
				_dialogue.Entries[i].EntryText += "\\p";
			};
			if (GUILayout.Button("Newline", GUILayout.MaxWidth(75)))
			{
				_dialogue.Entries[i].EntryText += "\\n";
			};
			if (GUILayout.Button("Pause", GUILayout.MaxWidth(75)))
			{
				_dialogue.Entries[i].EntryText += "\\.";
			};
			if (GUILayout.Button("4-Pause", GUILayout.MaxWidth(75)))
			{
				_dialogue.Entries[i].EntryText += "\\|";
			};
			EditorGUILayout.EndHorizontal();
			EditorGUILayout.BeginHorizontal();
			if (GUILayout.Button("Function", GUILayout.MaxWidth(75)))
			{
				_dialogue.Entries[i].EntryText += "\\>";
			};
			if (GUILayout.Button("Color", GUILayout.MaxWidth(75)))
			{
				_dialogue.Entries[i].EntryText += "\\c[TODO]";
			};
			if (GUILayout.Button("Face", GUILayout.MaxWidth(75)))
			{
				_dialogue.Entries[i].EntryText += "\\@TODO";
			};
			if (GUILayout.Button("Variable", GUILayout.MaxWidth(75)))
			{
				_dialogue.Entries[i].EntryText += "\\v[TODO]";
			};
			EditorGUILayout.EndHorizontal();

			EditorGUILayout.Space();

		} // end entry display

		// Add/remove buttons
		// TODO: add a way to swap/reorder: T item = base[oldIndex]; List.RemoveItem(oldIndex); List.InsertItem(newIndex, item);
		if (GUILayout.Button ("Add a new dialogue entry")) {
			DialogueEntry newEntry = ScriptableObject.CreateInstance("DialogueEntry") as DialogueEntry;
			newEntry.SpeakerName = "@unknown";
			newEntry.EntryText = "";
			_dialogue.Entries.Add(newEntry);
		}

		if (GUILayout.Button ("Delete the last entry")) {
			_dialogue.Entries.RemoveAt(_dialogue.Entries.Count - 1);
		}

	//	EditorGUI.indentLevel--;
		EditorGUILayout.EndVertical ();

		serializedObject.ApplyModifiedProperties();
	}

	// Takes a speakerName and returns its face sprite loaded from resources
	// note: not a recursive load, needs to be located in Resources/Sprites/Faces/
	private Sprite GetSpriteFromSpeakerName(string speakerName) {
		if (speakerName == "@unknown" || speakerName == "@ed") {
			// can skip load attempt if we know the speaker has no face
			return null;
		} else {
			// use @[name] and load the asset [name] from the faces folder
			return Resources.Load<Sprite>("Sprites/Faces/" + speakerName.Substring(1));
		}
	}

}
